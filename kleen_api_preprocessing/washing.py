"""Washing dataframe"""
import pandas as pd
import numpy as np

SIZE = 27

WASHING_MAPPING = {
    'Aspirateur': np.eye(1, SIZE, 0)[0],
    '3min Aspi': np.eye(1, SIZE, 0)[0],
    '3min Aspirateur - Soufflette': np.eye(1, SIZE, 0)[0],
    '3min Aspirateur': np.eye(1, SIZE, 0)[0],
    '3min Aspirateur Souffleur': np.eye(1, SIZE, 0)[0],
    '6min Aspirateur': np.eye(1, SIZE, 1)[0],
    '6min Aspirateur Souffleur': np.eye(1, SIZE, 1)[0],
    '9min Aspirateur': np.eye(1, SIZE, 2)[0],
    '9min Aspirateur Souffleur': np.eye(1, SIZE, 2)[0],
    '12min Aspirateur': np.eye(1, SIZE, 3)[0],
    '12min Aspirateur Souffleur': np.eye(1, SIZE, 3)[0],
    '15min Aspirateur': np.eye(1, SIZE, 4)[0],
    '15min Aspirateur Souffleur': np.eye(1, SIZE, 4)[0],
    '1.5min Lave-tapis': np.eye(1, SIZE, 5)[0],
    '3min Lave-tapis': np.eye(1, SIZE, 6)[0],
    '6min Lave-tapis': np.eye(1, SIZE, 7)[0],
    '3min HP (Uniquement pour les motos et les scooters)': np.eye(1, SIZE, 8)[0],
    '3min HP (2 roues)': np.eye(1, SIZE, 8)[0],
    '6min HP (2 roues)': np.eye(1, SIZE, 9)[0],
    '3 mins HP': np.eye(1, SIZE, 10)[0],
    '3min HP': np.eye(1, SIZE, 10)[0],
    '6min HP': np.eye(1, SIZE, 11)[0],
    '9min HP': np.eye(1, SIZE, 12)[0],
    '12min HP': np.eye(1, SIZE, 13)[0],
    '15min HP': np.eye(1, SIZE, 14)[0],
    '20 sec Lave-glaces': np.eye(1, SIZE, 15)[0],
    '15sec Parfumeur': np.eye(1, SIZE, 16)[0],
    '3min Gonfleur': np.eye(1, SIZE, 17)[0],
    'Programme 1': np.eye(1, SIZE, 18)[0],
    'P1': np.eye(1, SIZE, 18)[0],
    'Programme 2': np.eye(1, SIZE, 19)[0], 
    'P2': np.eye(1, SIZE, 19)[0], 
    'Programme 3': np.eye(1, SIZE, 20)[0],
    'P3': np.eye(1, SIZE, 20)[0],
    'Programme 4': np.eye(1, SIZE, 21)[0],
    'P4': np.eye(1, SIZE, 21)[0],
    'Programme 5': np.eye(1, SIZE, 22)[0],
    'P5': np.eye(1, SIZE, 22)[0],
    'Lavage Bronze': np.eye(1, SIZE, 23)[0],
    'Programme Bronze': np.eye(1, SIZE, 23)[0],
    'Lavage Silver': np.eye(1, SIZE, 24)[0],
    'Programme Silver': np.eye(1, SIZE, 24)[0],
    'Lavage Gold': np.eye(1, SIZE, 25)[0],
    'Programme Gold': np.eye(1, SIZE, 25)[0],
    'Lavage Platinium': np.eye(1, SIZE, 26)[0],
    'Programme Platinium': np.eye(1, SIZE, 26)[0],
}

WASHING_LABELS = [
    '3min_vacuum',
    '6min_vacuum',
    '9min_vacuum',
    '12min_vacuum',
    '15min_vacuum',
    '1.5min_mat_cleaner',
    '3min_mat_cleaner',
    '6min_mat_cleaner',
    '3min_high_pressure_two_wheeled',
    '6min_high_pressure_two_wheeled',
    '3min_high_pressure',
    '6min_high_pressure',
    '9min_high_pressure',
    '12min_high_pressure',
    '15min_high_pressure',
    'windshield_washer',
    'perfumer',
    'tire_inflator',
    'P1',
    'P2', 
    'P3',
    'P4',
    'P5',
    'tunnel_bronze',
    'tunnel_silver',
    'tunnel_gold',
    'tunnel_platinium'
]



def create_washing_df(order_df: pd.DataFrame, orderitem_df: pd.DataFrame) -> pd.DataFrame:
    """This function creates the washing dataframe.

    Parameters
    ----------
    order_df: pd.DataFrame
        preprocessed order dataframe from queries.get_processed_order_df
    orderitem_df: pd.DataFrame
        preprocessed orderitem dataframe from queries.get_processed_orderitem_df

    Return
    ------
    pd.DataFrame
        washing_df
    """
    washing_order_df = orderitem_df[orderitem_df["item.orderitem_type"] == "washing"]

    data = np.array(washing_order_df["item.washing_name"].map(WASHING_MAPPING).values.tolist())

    WASHING_QUANTITY_COLUMNS = ["washing.quantity.{k}".format(k=k) for k in WASHING_LABELS]
    washing_categories = pd.DataFrame(
        data = data,
        columns = WASHING_QUANTITY_COLUMNS, 
        index = washing_order_df.index
    )

    WASHING_PRICE_BEFORE_COLUMNS = ["washing.price_before_discount.{k}".format(k=k) for k in WASHING_LABELS]
    washing_price_before = pd.DataFrame(
        data = np.array([washing_order_df["item.price_before_discount"].values]).transpose()*data,
        columns = WASHING_PRICE_BEFORE_COLUMNS, 
        index = washing_order_df.index
    )

    WASHING_PRICE_AFTER_COLUMNS = ["washing.price_after_discount.{k}".format(k=k) for k in WASHING_LABELS]
    washing_price_after = pd.DataFrame(
        data = np.array([washing_order_df["item.price_after_discount"].values]).transpose()*data,
        columns = WASHING_PRICE_AFTER_COLUMNS, 
        index = washing_order_df.index
    )

    washing_df = pd.concat((washing_categories, washing_price_before, washing_price_after), axis=1)

    washing_df.loc[:, "washing.order_id"] = washing_order_df["item.order_id"]
    washing_df.loc[:, "washing.item_id"] = washing_order_df["item.id"]
    washing_df.loc[:, "washing.station_nf"] = washing_order_df["item.station_nf"]
    washing_df.loc[:, "washing.station_pk"] = washing_order_df["item.station_pk"]
    
    washing_df = washing_df.groupby(["washing.order_id"]).agg({
    **{
        "washing.{var}.{prog}".format(var = var, prog=prog): 'sum'
         for var in ["quantity", "price_before_discount", "price_after_discount"]
         for prog in WASHING_LABELS
    },
    **{
        'washing.item_id': lambda x: list(x), 
        'washing.station_nf': 'first', 
        'washing.station_pk': 'first'
    }
    }).reset_index()
    
    washing_df = order_df.merge(washing_df, how='right', left_on="order.id", right_on='washing.order_id')
    washing_df = washing_df.drop(columns=["washing.order_id"])

    return washing_df