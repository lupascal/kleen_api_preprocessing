"""Booking dataframe"""
import pandas as pd
import numpy as np

BOOKING_SIZE = 8

BOOKING_MAPPING = {
    None: np.eye(1, BOOKING_SIZE, 0)[0],
    'Dépoussiérage intérieur': np.eye(1, BOOKING_SIZE, 1)[0],
    'Lavage complet': np.eye(1, BOOKING_SIZE, 2)[0],
    "Véhicule très sale ou poils d'animaux": np.eye(1, BOOKING_SIZE, 3)[0],
    'Pressing sièges (ou Baume Cuir) & Moquettes': np.eye(1, BOOKING_SIZE, 4)[0],
    'Désinfection Habitacle Ultraviolets': np.eye(1, BOOKING_SIZE, 5)[0],
    'Baume Cuir': np.eye(1, BOOKING_SIZE, 6)[0],
    'Lavage intérieur': np.eye(1, BOOKING_SIZE, 7)[0],
}

BOOKING_COLUMNS = [
    'undetermined',
    'interior_dusting',
    'complete_wash',
    'dirty_vehicle',
    'pressing_seats_carpets',
    'ultraviolet_disinfection',
    'leather_balm',
    'interior_wash',
]

def create_booking_df(order_df: pd.DataFrame, orderitem_df: pd.DataFrame) -> pd.DataFrame:
    """This function creates the booking dataframe.

    Parameters
    ----------
    order_df: pd.DataFrame
        preprocessed order dataframe from queries.get_processed_order_df
    orderitem_df: pd.DataFrame
        preprocessed orderitem dataframe from queries.get_processed_orderitem_df

    Return
    ------
    pd.DataFrame
        booking_df
    """
    booking_order_df = orderitem_df[orderitem_df["item.orderitem_type"] == "booking"]

    data = np.array(booking_order_df["item.washing_name"].map(BOOKING_MAPPING).values.tolist())

    BOOKING_QUANTITY_COLUMNS = ["booking.quantity.{k}".format(k=k) for k in BOOKING_COLUMNS]
    booking_categories = pd.DataFrame(
        data = data,
        columns = BOOKING_QUANTITY_COLUMNS, 
        index = booking_order_df.index
    )

    BOOKING_PRICE_BEFORE_COLUMNS = ["booking.price_before_discount.{k}".format(k=k) for k in BOOKING_COLUMNS]
    booking_price_before = pd.DataFrame(
        data = np.array([booking_order_df["item.price_before_discount"].values]).transpose()*data,
        columns = BOOKING_PRICE_BEFORE_COLUMNS, 
        index = booking_order_df.index
    )

    BOOKING_PRICE_AFTER_COLUMNS = ["booking.price_after_discount.{k}".format(k=k) for k in BOOKING_COLUMNS]
    booking_price_after = pd.DataFrame(
        data = np.array([booking_order_df["item.price_after_discount"].values]).transpose()*data,
        columns = BOOKING_PRICE_AFTER_COLUMNS, 
        index = booking_order_df.index
    )

    booking_df = pd.concat((booking_categories, booking_price_before, booking_price_after), axis=1)

    booking_df.loc[:, "booking.booking_dropoff_time"] = booking_order_df["item.booking_dropoff_time"].apply(lambda x: str(x)[9:-1])
    booking_df.loc[:, "booking.booking_state"] = booking_order_df["item.booking_state"]
    booking_df.loc[:, "booking.station_pk"] = booking_order_df["item.station_pk"]
    booking_df.loc[:, "booking.order_id"] = booking_order_df["item.order_id"]

    # Define aggregation functions for each column
    aggregations = {
        **{
            "booking.{var}.{prog}".format(var = var, prog=prog): 'sum'
             for var in ["quantity", "price_before_discount", "price_after_discount"]
             for prog in BOOKING_COLUMNS
        },
        **{
            "booking.booking_dropoff_time": "first",
            "booking.booking_state": "first",
            "booking.station_pk": "first",
        }
    }

    booking_df = booking_df.groupby(["booking.order_id"]).agg(aggregations).reset_index()
    
    booking_df = order_df.merge(booking_df, how='right', left_on="order.id", right_on='booking.order_id')
    booking_df = booking_df.drop(columns=["booking.order_id"])
        
    return booking_df