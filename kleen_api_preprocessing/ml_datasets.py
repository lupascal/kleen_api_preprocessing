"""Generate machine learning datasets"""
import pandas as pd
import numpy as np
import os

from kleen_api_preprocessing.database import Database, KLEEN_API_SETTINGS
from kleen_api_preprocessing.queries import get_user_df, get_preprocessed_order_df, get_preprocessed_orderitem_df
from kleen_api_preprocessing.credits import create_credits_df
from kleen_api_preprocessing.washing import create_washing_df
from kleen_api_preprocessing.booking import create_booking_df


def to_csv(df: pd.DataFrame, output: str) -> None:
    """This function exports dataframe to csv and replaces 0 and NaN by null.

    Parameters
    ----------
    df: pd.DataFrame
        dataframe
    output: str
        output path
    """
    df.replace(np.nan, 0, inplace=True)
    df.replace(0, None, inplace=True)
    df.to_csv(output, index=False)


def generate_datasets(start_date: str) -> None:
    """This function generates the user, credits, washing and booking dataframes

    Parameters
    ----------
    start_date: str
        start date
    """
    db = Database(**KLEEN_API_SETTINGS)

    user_df = get_user_df(db)

    order_df = get_preprocessed_order_df(db, start_date)
    orderitem_df = get_preprocessed_orderitem_df(db, start_date)

    credits_df = create_credits_df(order_df, orderitem_df)
    washing_df = create_washing_df(order_df, orderitem_df)
    booking_df = create_booking_df(order_df, orderitem_df)

    return user_df, credits_df, washing_df, booking_df


def generate_datasets_to_csv(start_date: str, output_path: str = "") -> None:
    """This function generates the user, credits, washing and booking dataframes
    and save them in csv.

    Parameters
    ----------
    start_date: str
        start date
    output_path: str
        output_path
    """
    user_df, credits_df, washing_df, booking_df = generate_datasets(start_date)
    #user_df.to_csv(os.path.join(output_path, "df_users.csv"), index=False)
    to_csv(user_df, os.path.join(output_path, "df_users.csv"))
    to_csv(credits_df, os.path.join(output_path, "df_credits.csv"))
    to_csv(washing_df, os.path.join(output_path, "df_washing.csv"))
    to_csv(booking_df, os.path.join(output_path, "df_booking.csv"))


if __name__ == '__main__':
    from datetime import datetime

    if not os.path.isdir("..\..\datasets"):
        os.makedirs("..\..\datasets")
    
    c1 = datetime.now()
    generate_datasets_to_csv("2020-06-01", "..\..\datasets")
    c2 = datetime.now()

    print("END:", c2 - c1)