"""Credits dataframe"""
import pandas as pd
import numpy as np

CREDITS_SIZE = 7

CREDITS_MAPPING = {
    0: np.eye(1, CREDITS_SIZE, 0)[0],
    10: np.eye(1, CREDITS_SIZE, 1)[0],
    15: np.eye(1, CREDITS_SIZE, 2)[0],
    25: np.eye(1, CREDITS_SIZE, 3)[0],
    35: np.eye(1, CREDITS_SIZE, 4)[0],
    65: np.eye(1, CREDITS_SIZE, 5)[0],
    100:  np.eye(1, CREDITS_SIZE, 6)[0],
}

CREDITS_PRICE = [0, 10, 15, 25, 35, 65, 100]

CREDITS_COLUMNS = ["credits.pack_{k}".format(k=k) for k in CREDITS_PRICE]


def create_credits_df(order_df: pd.DataFrame, orderitem_df: pd.DataFrame) -> pd.DataFrame:
    """This function creates the credits dataframe.

    Parameters
    ----------
    order_df: pd.DataFrame
        preprocessed order dataframe from queries.get_processed_order_df
    orderitem_df: pd.DataFrame
        preprocessed orderitem dataframe from queries.get_processed_orderitem_df

    Return
    ------
    pd.DataFrame
        credits_df
    """
    credits_order_df = orderitem_df[orderitem_df["item.orderitem_type"] == "credits_pack"]
    credits_data = np.array(credits_order_df["item.credits_pack_price"].map(CREDITS_MAPPING).values.tolist())

    credits_df = pd.DataFrame(
        data = credits_data,
        columns = CREDITS_COLUMNS, 
        index = credits_order_df.index
    )
    credits_df.loc[:, "credits.price_after_discount"] = (
        credits_order_df["item.price_after_discount"] 
        * (credits_order_df["item.credits_pack_price"] != 0.)
    )
    credits_df.loc[:, "credits.price_before_discount"] = (
        credits_order_df["item.price_before_discount"] 
        * (credits_order_df["item.credits_pack_price"] != 0.)
    )

    credits_df.loc[:, "credits.station_pk"] = credits_order_df["item.station_pk"]
    credits_df.loc[:, "order_id"] = credits_order_df['item.order_id']
    credits_df.loc[:, "credits.item_id"] = credits_order_df['item.id']

    credits_df = credits_df.groupby(["order_id"]).agg({
    **{
        "credits.pack_{k}".format(k=k): "sum"
         for k in CREDITS_MAPPING.keys()
    },
    **{
        'credits.price_after_discount': 'sum',
        'credits.price_before_discount': 'sum',
        'credits.item_id': lambda x: list(x), 
        'credits.station_pk': 'first'
    }
    }).reset_index()
    
    credits_df = order_df.merge(credits_df, how='right', left_on="order.id", right_on='order_id')

    # Correction sur les recharges prepayees, aucun prix n'est payé dans l'app
    credits_df.loc[credits_df["order.is_station_prepaid_credits"], "credits.price_after_discount"] = 0
    credits_df.loc[credits_df["order.is_station_prepaid_credits"], "credits.price_before_discount"] = 0

    credits_df.loc[:, "credits.voucher_offered_credits"] = (
        credits_order_df["item.price_before_discount"] 
        * (credits_order_df["item.credits_pack_price"] == 0.)
    )
    credits_df.loc[:, "credits.total_offered_credits"] = (
        credits_df["credits.price_before_discount"] 
        - credits_df["credits.price_after_discount"] 
        + credits_df["credits.voucher_offered_credits"]
    )
    
    return credits_df