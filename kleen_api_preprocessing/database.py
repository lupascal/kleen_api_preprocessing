"""Database"""
import psycopg2
import psycopg2.extras
import pandas as pd

# Global settings to import
KLEEN_API_SETTINGS = {
    "host": "localhost",
    "database": "kleen_api",
    "user": "root",
    "password": "root",
    "port": "5433",
    "dict_output": True,
}

# Global settings to import
METEOMATICS_SETTINGS = {
    "host": "localhost",
    "database": "meteomatics",
    "user": "root",
    "password": "root",
    "port": "5436",
    "dict_output": True,
}

AUTOWASH_REF_SETTINGS = {
    "host": "localhost",
    "database": "autowash_ref",
    "user": "root",
    "password": "root",
    "port": "5434",
    "dict_output": True,
}

# Global settings to import
SALE_INFORMATION_SETTINGS = {
    "host": "localhost",
    "database": "sale_information",
    "user": "root",
    "password": "root",
    "port": "5435",
    "dict_output": True,
}

# Global settings to import
POWERBI_BRIDGE_SETTINGS = {
    "host": "localhost",
    "database": "powerbi_bridge",
    "user": "root",
    "password": "root",
    "port": "5437",
    "dict_output": True,
}


class Database:  # pylint:disable=too-many-instance-attributes
    """Database"""

    def __init__(self, **kwargs):
        self.host = kwargs.get("host", "localhost")
        self.database = kwargs["database"]
        self.user = kwargs.get("user", "root")
        self.password = kwargs.get("password", "root")
        self.port = kwargs["port"]
        self.dict_ouput = kwargs.get("dict_output", None)

        self.conn = None
        self.cursor = None

        self._connection()
        self._new_cursor()

    def _connection(self):
        print("Connecting to the PostgreSQL database... \n")
        try:
            self.conn = psycopg2.connect(
                host=self.host,
                database=self.database,
                user=self.user,
                password=self.password,
                port=self.port,
            )
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def _new_cursor(self):
        if self.dict_ouput:
            self.cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        else:
            self.cursor = self.conn.cursor()

    def query(self, query, query_params=None):
        try:
            self.cursor.execute(query, query_params)
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

        return self.cursor

    def dataframe(self, query, query_params=None, **kwargs):
        """Returns a DataFrame corresponding to the result set of the query string.
        For more informatio:
        https://pandas.pydata.org/docs/reference/api/pandas.read_sql_query.html
        """
        return pd.read_sql_query(query, self.conn, params=query_params, **kwargs)

    def close(self):
        if self.cursor:
            self.cursor.close()
        if self.conn:
            self.conn.close()
        print("Database connection closed. \n")
