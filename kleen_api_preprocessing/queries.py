"""SQL queries"""
import pandas as pd
import numpy as np
from kleen_api_preprocessing.database import Database


PREPAID_CREDIT_ID = [90, 89, 88, 87, 86, 85]

CUSTOMER_SERVICE_ID = [
    26, 124, 104, 4, 10, 156, 158, 3, 5, 32, 33, 7, 2, 6, 8, 9, 17, 91, 
    93, 138, 92, 94, 101, 102, 108, 110, 120, 109, 111, 112, 113, 114,
    121, 125, 127, 129, 131, 126, 128, 130, 132, 133, 134, 139, 141, 
    143, 140, 142, 144, 145, 148, 149, 150, 151, 152, 164, 165, 166, 
    167, 168, 169, 170, 171, 172, 173, 174, 178, 11, 123
]


def get_user_df(db: Database) -> pd.DataFrame:
    """This function queries the accounts_user table of kleen_api.
    Active users only.

    Parameters
    ----------
    db: Database
        database

    Return
    ------
    pd.DataFrame
        user_df
    """
    query = "SELECT id, uuid, email, birthday, system, is_frauder, is_active, \
    is_fleet_owner, allow_crm, allow_newsletter, date_joined, date_deleted, \
    billing_information_id FROM accounts_user"

    user_df = db.dataframe(query)
    user_df.columns = ["user.{k}".format(k=k) for k in user_df.columns]
    user_df["user.has_billing_information"] = user_df["user.billing_information_id"].notnull()
    user_df.drop(columns = ["user.billing_information_id"])
    user_df.replace({False: 0, True: 1}, inplace=True)
    user_df = user_df[user_df["user.email"].notnull()]

    return user_df


def get_order_df(db: Database, date: str) -> pd.DataFrame:
    """This function queries the carwash_order table of kleen_api.

    Parameters
    ----------
    db: Database
        database
    date: str
        start date

    Return
    ------
    pd.DataFrame
        order_df
    """
    query = "SELECT id, status, sold_date, sold_price, promocode_code, promocode_price,\
    user_id, user_email, user_uuid, promocode_used_price, promocode_type, prepaid_account_id, \
    prepaid_account_price_incl_vat, prepaid_account_used_incl_vat \
    FROM carwash_order WHERE sold_date >= '{date}' ORDER BY sold_date".format(date=date)

    order_df = db.dataframe(query)
    order_df.columns = ["order.{k}".format(k=k) for k in order_df.columns]
    order_df['order.promocode_prefix'] = order_df["order.promocode_code"].str.slice(0, 3)
    
    return order_df


def get_carwash_checkout_df(db: Database) -> pd.DataFrame:
    """This function queries the carwash_checkout table of kleen_api.

    Parameters
    ----------
    db: Database
        database

    Return
    ------
    pd.DataFrame
        carwash_checkout_df
    """
    query = "SELECT id, order_id, payment_method FROM carwash_checkout"
    checkout_df = db.dataframe(query)
    checkout_df.columns = ["checkout.{k}".format(k=k) for k in checkout_df.columns]
    
    return checkout_df


def get_promocode_df(db: Database) -> pd.DataFrame:
    """This function queries the carwash_promocode table of kleen_api.

    Parameters
    ----------
    db: Database
        database

    Return
    ------
    pd.DataFrame
        carwash_promocode_df
    """
    query = "SELECT id, code, price, lifetime, start_date, code_type, promotional_campaign_id, \
    credits_pack_to_offer_id, source_station FROM carwash_promocode"
    promocode_df = db.dataframe(query)
    promocode_df.columns = ["promocode.{k}".format(k=k) for k in promocode_df.columns]
    return promocode_df


def get_partners_promocampaign_df(db: Database) -> pd.DataFrame:
    """This function queries the partners_promotionalcampaign table of kleen_api.

    Parameters
    ----------
    db: Database
        database

    Return
    ------
    pd.DataFrame
        partners_promotionalcampaign_df
    """
    query = "SELECT id, title FROM partners_promotionalcampaign"
    promocampaign_df = db.dataframe(query)
    promocampaign_df.columns = ["promocampaign.{k}".format(k=k) for k in promocampaign_df.columns]
    return promocampaign_df


def get_credit_pack_values_df(db: Database) -> pd.DataFrame:
    """This function queries the carwash_deal table of kleen_api.

    Parameters
    ----------
    db: Database
        database

    Return
    ------
    pd.DataFrame
        carwash_deal
    """
    query = "SELECT credits_pack_id, price_before_discount FROM carwash_deal WHERE credits_pack_id > 0"
    deal_df = db.dataframe(query)
    deal_df.columns = ["deal.{k}".format(k=k) for k in deal_df.columns]
    deal_df = deal_df.drop_duplicates().reset_index(drop=True)
    return deal_df


def get_orderitem_df(db: Database, date: str) -> pd.DataFrame:
    """This function queries the carwash_orderitem table of kleen_api.

    Parameters
    ----------
    db: Database
        database
    date: str
        start date

    Return
    ------
    pd.DataFrame
        carwash_orderitem_df
    """
    query = "SELECT id, data, orderitem_type, order_id, \
    price_after_discount, price_before_discount, creation_time, credits_pack_price \
    FROM carwash_orderitem WHERE creation_time >= '{date}' ORDER BY creation_time".format(date=date)

    orderitem_df = db.dataframe(query)
    orderitem_df.columns = ["item.{k}".format(k=k) for k in orderitem_df.columns]
    return orderitem_df


def get_preprocessed_orderitem_df(db: Database, date: str) -> pd.DataFrame:
    """This function queries the carwash_orderitem table of kleen_api, then preprocesses it

    Parameters
    ----------
    db: Database
        database
    date: str
        start date

    Return
    ------
    pd.DataFrame
        orderitem_df
    """
    orderitem_df = get_orderitem_df(db, date)
    washing_type = []
    washing_name = []
    booking_state = []
    credits_pack_id = []
    station_nf = []
    station_pk = []
    booking_dropoff_time = []

    for k in range(orderitem_df.shape[0]):
        order_type = orderitem_df["item.orderitem_type"].iloc[k]
        raw_data = orderitem_df["item.data"].iloc[k]

        if order_type == "washing":
            washing_type.append(raw_data["washingprogram_washing_type"])
            washing_name.append(raw_data["washingprogram_name"])
            station_pk.append(raw_data["station_pk"])
            booking_state.append(None)
            credits_pack_id.append(None)
            booking_dropoff_time.append(None)

            if "station_code_nf" in raw_data:
                station_nf.append(raw_data["station_code_nf"])
            else:
                station_nf.append(None)

        elif order_type == "booking":
            washing_type.append(None)
            washing_name.append(None)
            credits_pack_id.append(None)
            station_nf.append(None)
            booking_state.append(raw_data["booking_state"])
            station_pk.append(raw_data["station_pk"])
            booking_dropoff_time.append(raw_data["booking_dropoff_time"])

        elif order_type == "credits_pack":
            id_ = raw_data["credits_pack_id"]
            credits_pack_id.append(id_)
            washing_type.append(None)
            washing_name.append(None)
            booking_state.append(None)
            booking_dropoff_time.append(None)

            if "station_code_nf" in raw_data:
                station_nf.append(raw_data["station_code_nf"])
            else:
                station_nf.append(None)

            if "station_pk" in raw_data:
                station_pk.append(raw_data["station_pk"])
            else:
                station_pk.append(None)
        else: 
            print("unknown", raw_data["credits_pack_id"])

    orderitem_df["item.washing_type"] = washing_type
    orderitem_df["item.washing_name"] = washing_name
    orderitem_df["item.booking_state"] = booking_state
    orderitem_df["item.credits_pack_id"] = credits_pack_id
    orderitem_df["item.station_nf"] = station_nf
    orderitem_df["item.station_pk"] = station_pk
    orderitem_df["item.booking_dropoff_time"] = booking_dropoff_time
    
    return orderitem_df


def get_preprocessed_order_df(db: Database, date: str) -> pd.DataFrame:
    """This function queries the carwash_order table of kleen_api, then preprocesses it

    Parameters
    ----------
    db: Database
        database
    date: str
        start date

    Return
    ------
    pd.DataFrame
        order_df
    """
    order_df = get_order_df(db, date)
    checkout_df = get_carwash_checkout_df(db)
    order_df = order_df.merge(
        checkout_df, left_on="order.id", right_on="checkout.order_id", how="left"
    )
    promocode_df = get_promocode_df(db)
    promocampaign_df = get_partners_promocampaign_df(db)

    order_df.loc[:, "order.payment_method"] = order_df.loc[:, "checkout.payment_method"]
    order_df.loc[:, "order.sold_price"] = order_df.loc[:, "order.sold_price"].fillna(0)
    order_df.loc[:, "order.prepaid_account_used_incl_vat"] = (
        order_df.loc[:, "order.prepaid_account_used_incl_vat"].fillna(0)
    )
    order_df.loc[:, "order.promocode_price"] = order_df.loc[:, "order.promocode_price"].fillna(0)

    # Paiement method
    for payment_method in ["mangopay", "payline_total_gr", "twip_free", "prepaid_account_mangopay"]:
        order_df.loc[:, 'order.{payment_method}'.format(payment_method = payment_method)] = (
            order_df.loc[:, "order.sold_price"] * (order_df["checkout.payment_method"] == payment_method)
        )

    order_df.loc[:, "order.total_price"] = (
        order_df.loc[:, "order.sold_price"] + 
        order_df.loc[:, "order.prepaid_account_used_incl_vat"]
    )

    # Promotions
    order_df = order_df.merge(
        promocode_df, 
        how="left",
        left_on="order.promocode_code",
        right_on="promocode.code"
    )
    order_df = order_df.merge(
        promocampaign_df, 
        how="left",
        left_on="promocode.promotional_campaign_id",
        right_on="promocampaign.id"
    )

    order_df = order_df[[
        "order.user_id",
        "order.id",
        "order.status",
        "order.sold_date",
        "order.payment_method",
        "order.mangopay",
        "order.payline_total_gr",
        "order.twip_free",
        "order.prepaid_account_mangopay",
        "order.sold_price",
        "order.prepaid_account_used_incl_vat",
        "order.total_price",
        "order.promocode_code",
        "order.promocode_price",
        "promocode.promotional_campaign_id",
        "promocampaign.title",
        "promocode.source_station",
    ]]

    order_df.columns = [
        "order.user_id",
        "order.id",
        "order.status",
        "order.sold_date",
        "order.payment_method",
        "order.mangopay",
        "order.payline_total_gr",
        "order.twip_free",
        "order.prepaid_account_mangopay",
        "order.sold_price",
        "order.prepaid_account_used_incl_vat",
        "order.total_price",
        "order.promocode_code",
        "order.promocode_price",
        "order.promotional_campaign_id",
        "order.promotional_campaign_title",
        "order.source_station",
    ]

    order_df.loc[:, "order.is_station_prepaid_credits"] = (
        order_df["order.promotional_campaign_id"].isin(PREPAID_CREDIT_ID)
    )
    order_df.loc[:, "order.is_customer_service_code"] = (
        order_df["order.promotional_campaign_id"].isin(CUSTOMER_SERVICE_ID)
    )
    order_df.loc[:, "order.is_promotional_code"] = ~(
        (order_df["order.promotional_campaign_id"].isnull()) | 
        (order_df["order.promotional_campaign_id"].isin(PREPAID_CREDIT_ID)) | 
        (order_df["order.promotional_campaign_id"].isin(CUSTOMER_SERVICE_ID))
    )

    order_df.loc[:, "order.promocode_price"] = order_df["order.promocode_price"].fillna(0)
    order_df.loc[:, "order.promotional_campaign_id"] = order_df["order.promotional_campaign_id"].fillna(value=np.nan)
    order_df.loc[:, "order.promotional_campaign_title"] = order_df["order.promotional_campaign_title"].fillna(value=np.nan)

    order_df.replace(np.nan, None, inplace=True)
    order_df.replace(0, None, inplace=True)
    
    return order_df