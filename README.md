# Kleen-api Preprocessing

This library intends to preprocess the kleen_api database and returns useful tables ready for machine learning. These tables include:
* the users table,
* the washing orders table,
* the credits orders table,
* the booking orders table.

The input database must be downloaded from AWS S3.


## Authors and acknowledgment

- Pascal LU <pascal.lu@wash.totalenergies.com>



## Run the code

`pip install -e .`

Then:

`from kleen_api_preprocessing import ml_datasets

ml_datasets.generate_datasets_to_csv("2020-06-01", "..\datasets")`



## Known issues due to kleen_api database.

* Incoherence between price_after_discount and total_price (shoud be equal).
* Incoherence of promocode_price variable.
* Incoherence of paiement method in case where there are two paiement methods (credit card + wallet or Fleet card + wallet). It is not possible to determine the second part.
* Incoherence of carwash_deal credit pack values, as several pack ids can point to different values.
* Prepaid station credits have been fixed: price_before_discount and price_after_discount are set at 0.